//
//  Flight.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation

struct Flight {
	let number: String
	let origin: String
	let destination: String
	let departure: Date
	let arrival: Date
	let moreData: String?
	let evenMoreData: String?
}
