//
//  AppDelegate.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		window = UIWindow(frame: UIScreen.main.bounds)
		let rootViewController = ResultsAssembler.makeModule() as! UIViewController
		window?.rootViewController = UINavigationController(rootViewController: rootViewController)
		window?.makeKeyAndVisible()
		
		return true
	}

}
