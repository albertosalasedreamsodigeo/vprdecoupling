//
//  UIViewController+BaseView.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: NavigableView {
	
	func push(view: BaseView) {
		if let nc = self.navigationController {
			nc.pushViewController(view as! UIViewController, animated: true)
		} else {
			self.present(view as! UIViewController, animated: true)
		}
	}
	
	func popView() {
		if let nc = self.navigationController {
			nc.popViewController(animated: true)
		}
	}
	
	func popToRootView() {
		if let nc = self.navigationController {
			nc.popToRootViewController(animated: true)
		}
	}
	
}

// Replace with your own implementation
extension UIViewController: AlertableView {
	
	func showInfo(message: String, completion: (() -> Void)?) {
		let title = NSLocalizedString("Info", comment: "Info alert title")
		let ok = NSLocalizedString("Ok", comment: "Ok alert button")
		showAlertView(title: title, message: message, buttonLabel: ok, completion: completion)
	}
	
	func showError(_ error: LocalizedError, completion: (() -> Void)?) {
		let title = NSLocalizedString("Error", comment: "Error alert title")
		let ok = NSLocalizedString("Ok", comment: "Ok alert button")
		showAlertView(title: title, message: error.localizedDescription, buttonLabel: ok, completion: completion)
	}
	
	// Utils
	
	private func showAlertView(title: String, message: String, buttonLabel: String, completion: (() -> Void)? = nil) {
		
		var actionHandler: ((UIAlertAction) -> Void)? = nil
		if let completion = completion {
			actionHandler = { _ in
				completion()
			}
		}
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: buttonLabel, style: UIAlertActionStyle.default, handler: actionHandler))
		self.present(alert, animated: true, completion: nil)
	}
	
}

extension UIViewController: AsyncLoadableView {
	
	func startLoading() {
		// Use your own implementation
	}
	
	func stopLoading() {
		// Use your own implementation
	}
	
}
