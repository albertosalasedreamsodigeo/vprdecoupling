//
//  BaseView.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//

import Foundation

protocol BaseView: class {
	
}

protocol NavigableView: BaseView {
	func push(view: BaseView)
	func popToRootView()
	func popView()
}

protocol AlertableView: BaseView {
	func showInfo(message: String, completion: (() -> Void)?)
	func showError(_ error: LocalizedError, completion: (() -> Void)?)
}

protocol AsyncLoadableView: BaseView {
	func startLoading()
	func stopLoading()
}
