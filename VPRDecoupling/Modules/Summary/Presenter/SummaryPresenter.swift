//
//  SummaryPresenter.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.3 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class SummaryPresenter {

    weak var view: SummaryViewProtocol!
    var interactor: SummaryInteractorInputProtocol!
    var router: SummaryRouterProtocol!
    var params: SummaryParams?

}

extension SummaryPresenter: SummaryPresenterProtocol {

    func viewDidLoad() {
        if let params = params {
            let label = params.outboundFlight.number + " - " + params.inboundFlight.number
            let viewModel = SummaryViewModel(title: "Summary", labelText: label)
            view.viewModel = viewModel
        }
    }

}

extension SummaryPresenter: SummaryInteractorOutputProtocol {

}
