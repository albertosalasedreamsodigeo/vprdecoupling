//
//  SummaryAssembler.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation

class SummaryAssembler {
	
	static func makeModule(params: SummaryParams? = nil) -> BaseView {
		
		// Instantiation
		let view = SummaryViewController()
		let presenter = SummaryPresenter()
		let router = SummaryRouter()
		let interactor = SummaryInteractor()
		
		// View - Presenter
		view.presenter = presenter
		presenter.view = view
		
		// View/Presenter - Router
		router.view = view
		presenter.router = router
		
		// Presenter - Interactor
		presenter.interactor = interactor
		interactor.presenter = presenter
		
		// Params
		presenter.params = params
		
		return view
	}
	
}
