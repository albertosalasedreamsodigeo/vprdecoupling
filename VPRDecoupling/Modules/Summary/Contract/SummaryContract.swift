//
//  SummaryContract.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.3 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

//MARK: - Router
protocol SummaryRouterProtocol: class {

}

//MARK: - View
protocol SummaryViewProtocol: BaseView {
	var viewModel: SummaryViewModel? { get set }
}

//MARK: - Presenter
protocol SummaryPresenterProtocol: class {
	func viewDidLoad()
}

//MARK: - Interactor
protocol SummaryInteractorInputProtocol: class {

}

protocol SummaryInteractorOutputProtocol: class {

}
