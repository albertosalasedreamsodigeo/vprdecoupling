//
//  SummaryParams.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.3 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

struct SummaryParams {
    
    var outboundFlight: Flight
    var inboundFlight: Flight

}
