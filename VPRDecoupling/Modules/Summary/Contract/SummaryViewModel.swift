//
//  SummaryViewModel.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.3 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class SummaryViewModel {
	
	let title: String
    let labelText: String
    
    init(title: String, labelText: String) {
        self.title = title
        self.labelText = labelText
    }

}
