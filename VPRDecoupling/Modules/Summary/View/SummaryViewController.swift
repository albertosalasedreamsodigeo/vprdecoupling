//
//  SummaryViewController.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.3 https://github.com/albsala/ASVIPERTemplate/
//

import UIKit

class SummaryViewController: UIViewController {

	var presenter: SummaryPresenterProtocol!
	var viewModel: SummaryViewModel? {
		didSet {
            if let viewModel = viewModel {
                title = viewModel.title
                label.text = viewModel.labelText
            }
		}
	}
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }

}

extension SummaryViewController: SummaryViewProtocol {

}
