//
//  ResultsPresenter.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class ResultsPresenter {

    weak var view: ResultsViewProtocol!
    var interactor: ResultsInteractorInputProtocol!
    var router: ResultsRouterProtocol!
    var params: ResultsParams?
	
	var outboundFlight: Flight? = nil
	var inboundFlight: Flight? = nil
    
    func updateContinueButton() {
        let enabled = outboundFlight != nil && inboundFlight != nil
        view.setContinueButtonEnabled(enabled)
    }

}

extension ResultsPresenter: ResultsPresenterProtocol {

    func viewDidLoad() {
		interactor.fetchFlights()
    }
	
	func outboundFlightSelected(_ flight: ResultsFlightViewModel) {
        if outboundFlight != nil {
            outboundFlight = nil
            view.showAllOutboundFlights()
            view.deselectOutboundFlight(flight)
            inboundFlight = nil
            view.showAllInboundFlights()
        } else if let resultsFlight = flight as? ResultsFlight {
			outboundFlight = resultsFlight.flight
			view.hideOutboundFlightsBut(flight)
			view.deselectOutboundFlight(flight)
		}
        updateContinueButton()
	}
	
	func inboundFlightSelected(_ flight: ResultsFlightViewModel) {
        
        guard outboundFlight != nil else {
            router.showError(message: "Select an outcome flight first")
            return
        }
        
        if inboundFlight != nil {
            inboundFlight = nil
            view.showAllInboundFlights()
            view.deselectInboundFlight(flight)
        } else if let resultsFlight = flight as? ResultsFlight {
			inboundFlight = resultsFlight.flight
			view.hideInboundFlightsBut(flight)
			view.deselectInboundFlight(flight)
		}
        updateContinueButton()
        
	}
	
	func continueClicked() {
        if let outF = outboundFlight, let inF = inboundFlight {
            router.showSummary(outboundFlight: outF, inboundFlight: inF)
        }
	}

}

extension ResultsPresenter: ResultsInteractorOutputProtocol {
	
	func didReceiveFlights(_ flights: ([Flight], [Flight])) {
		view.viewModel = ResultsViewModel(title: "Results", outboundHeader: "Outbound", outbound: flights.0, inboundHeader: "Inbound", inbound: flights.1)
        updateContinueButton()
	}

}


