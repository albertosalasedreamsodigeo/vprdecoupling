//
//  ResultsPresenter+ViewModel.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 9/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation

class ResultsFlight: ResultsFlightViewModel {
	
	let flight: Flight
	
	init(flight: Flight) {
		self.flight = flight
		super.init(number: flight.number, origin: flight.origin, destination: flight.destination, departure: flight.departure, arrival: flight.arrival)
	}
	
}

extension ResultsViewModel {
	
	convenience init(title: String?, outboundHeader: String, outbound: [Flight], inboundHeader: String, inbound: [Flight]) {
        
        let outboundFlights = outbound.map(ResultsFlight.init(flight:))
        let outboundVM = ResultsFlightListViewModel(header: outboundHeader, footer: nil, flights: outboundFlights)
        let inboundFlights = inbound.map(ResultsFlight.init(flight:))
        let inboundVM = ResultsFlightListViewModel(header: inboundHeader, footer: nil, flights: inboundFlights)
        self.init(title: title, outbound: outboundVM, inbound: inboundVM)
        
	}
	
}
