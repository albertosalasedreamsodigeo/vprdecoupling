//
//  ResultsRouter.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class ResultsRouter: ResultsRouterProtocol {
    
    weak var view: (NavigableView & AlertableView)!
    
    func showSummary(outboundFlight: Flight, inboundFlight: Flight) {
        let params = SummaryParams(outboundFlight: outboundFlight, inboundFlight: inboundFlight)
        let summaryView = SummaryAssembler.makeModule(params: params)
        view.push(view: summaryView)
    }
    
    func showError(message: String) {
        view.showInfo(message: message, completion: nil)
    }
	
}
