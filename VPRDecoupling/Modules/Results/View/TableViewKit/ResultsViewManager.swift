//
//  ResultsViewManager.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 10/01/2018.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation
import TableViewKit

enum ResultsFlightBound: Int {
    case inbound, outbound
}

protocol ResultsViewManagerDelegate: class {
    func flightSelected(bound: ResultsFlightBound, item: ResultsFlightViewModel)
    func continueButtonClick()
}

class ResultsViewManager: TableViewManager {
    
    weak var resultsDelegate: ResultsViewManagerDelegate?
    var outboundSection: LegSection? { return sections[0] as? LegSection }
    var inboundSection: LegSection? { return sections[1] as? LegSection }
    var buttonSection: ButtonSection? { return sections[2] as? ButtonSection }
    
    init(tableView: UITableView, delegate:ResultsViewManagerDelegate, viewModel: ResultsViewModel?) {
        self.resultsDelegate = delegate
        if let viewModel = viewModel {
            let outboundSection = LegSection(viewModel: viewModel.outbound!, bound: ResultsFlightBound.outbound)
            let inboundSection = LegSection(viewModel: viewModel.inbound!, bound: ResultsFlightBound.inbound)
            let continueSection = ButtonSection(buttonTitle: "Continue")
            super.init(tableView: tableView, sections: [outboundSection, inboundSection, continueSection])
            outboundSection.delegate = self
            inboundSection.delegate = self
            continueSection.delegate = self
        } else {
            super.init(tableView: tableView)
        }
    }
    
    func hideOutboundFlightsBut(_ flight: ResultsFlightViewModel) {
        outboundSection?.hideFlightsBut(flight)
    }
    
    func hideInboundFlightsBut(_ flight: ResultsFlightViewModel) {
        inboundSection?.hideFlightsBut(flight)
    }
    
    func showAllOutboundFlights() {
        outboundSection?.showAllItems()
    }
    
    func showAllInboundFlights() {
        inboundSection?.showAllItems()
    }
    
    func deselectOutboundFlight(_ flight: ResultsFlightViewModel) {
        outboundSection?.deselectFligth(flight)
    }
    
    func deselectInboundFlight(_ flight: ResultsFlightViewModel) {
        inboundSection?.deselectFligth(flight)
    }
    
    func setContinueButtonEnabled(_ enabled: Bool) {
        buttonSection?.setContinueButtonEnabled(enabled)
    }
    
}

extension ResultsViewManager: LegSectionDelegate {
    func flightSelected(legSection: LegSection, item: FlightItem) {
        resultsDelegate?.flightSelected(bound: legSection.bound, item: item.viewModel)
    }
}

extension ResultsViewManager: ButtonSectionDelegate {
    func buttonClick() {
        resultsDelegate?.continueButtonClick()
    }
}
