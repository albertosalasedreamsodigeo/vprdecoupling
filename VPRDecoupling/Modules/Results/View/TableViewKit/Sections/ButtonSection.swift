//
//  ButtonSection.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 7/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation
import TableViewKit

protocol ButtonSectionDelegate: class {
	func buttonClick()
}

class ButtonSection: TableSection {
	
	var items: ObservableArray<TableItem> = []
	
	weak var delegate: ButtonSectionDelegate?
	
	init(buttonTitle: String, delegate: ButtonSectionDelegate? = nil) {
		
		self.delegate = delegate
		
		items = [ButtonItem(title: buttonTitle, enabled: false, onClick: { [unowned self] in
			self.delegate?.buttonClick()
		})]
		
	}
	
	func setContinueButtonEnabled(_ enabled: Bool) {
		let buttonItem = items.first! as! ButtonItem
		buttonItem.enabled = enabled
		buttonItem.redraw()
	}
	
}
