//
//  LegSection.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 6/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import Foundation
import TableViewKit

protocol LegSectionDelegate: class {
	func flightSelected(legSection: LegSection, item: FlightItem)
}

class LegSection: TableSection, Stateful {
	
	var items: ObservableArray<TableItem> = []
	var header: HeaderFooterView = nil
	
    weak var delegate: LegSectionDelegate?
	let bound: ResultsFlightBound
	
	init(viewModel: ResultsFlightListViewModel, bound: ResultsFlightBound) {
		
		self.bound = bound
		
		if let vmHeader = viewModel.header {
			self.header = .title(vmHeader)
		}
		
		if let flights = viewModel.flights {
            self.allItems = flights.map { flightVM in
                let item = FlightItem(viewModel: flightVM)
                item.onSelection = { [unowned self] item in
                    self.delegate?.flightSelected(legSection: self, item: item)
                }
                return item
            }
			self.transition(to: currentState)
		}
		
	}
    
    func setDelegateForItems(delegate: LegSectionDelegate?, items: [FlightItem]) {
        items.forEach { item in
            item.onSelection = { item in
                delegate?.flightSelected(legSection: self, item: item)
            }
        }
    }
	
	func itemWithFlight(_ flight: ResultsFlightViewModel) -> FlightItem? {
		return allItems.first(where: { ($0 as! FlightItem).viewModel == flight }) as? FlightItem
	}
	
	func hideFlightsBut(_ flight: ResultsFlightViewModel) {
		if let flightItem = itemWithFlight(flight) {
			transition(to: .selected(flightItem))
		}
	}
    
    
    func showAllItems() {
        transition(to: .all)
    }
	
	func deselectFligth(_ flight: ResultsFlightViewModel) {
		if let flightItem = itemWithFlight(flight) {
			flightItem.deselect(animated: true)
		}
	}
	
	//MARK: - Stateful
	
    var allItems: [TableItem] = []
	
	enum State {
		case all
		case selected(TableItem)
	}
	var currentState: State = .all
	
	func items(for state: State) -> [TableItem] {
		switch state {
		case .all:
			return allItems
		case .selected(let item):
			return [item]
		}
	}
	
}
