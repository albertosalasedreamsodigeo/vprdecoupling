//
//  FlightItem.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 7/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import TableViewKit

class FlightItem: TableItem, Selectable {
	
	static var drawer = AnyCellDrawer(FlightDrawer.self)
	var viewModel: ResultsFlightViewModel
	
	init(viewModel: ResultsFlightViewModel) {
		self.viewModel = viewModel
	}
	
	//MARK: - Selectable
	var onSelection: (FlightItem) -> Void = { _ in }
	func didSelect() {
		onSelection(self)
	}
}

class FlightDrawer: CellDrawer {
	
	static var nib = UINib(nibName: "FlightCell", bundle: nil)
	static var type = CellType.nib(nib, FlightCell.self)
	
	static var dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .short
		dateFormatter.timeStyle = .short
		return dateFormatter
	}()
	
	static func draw(_ cell: FlightCell, with item: FlightItem) {
		cell.flightNumber.text = item.viewModel.number
		cell.origin.text = item.viewModel.origin
		cell.destination.text = item.viewModel.destination
		cell.departure.text = dateFormatter.string(from: item.viewModel.departure)
		cell.arrival.text = dateFormatter.string(from: item.viewModel.arrival)
	}
}
