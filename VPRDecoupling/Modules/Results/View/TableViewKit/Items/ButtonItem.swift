//
//  ButtonItem.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 7/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import TableViewKit

class ButtonItem: TableItem {
	
	static var drawer = AnyCellDrawer(ButtonDrawer.self)
	var title: String
	var enabled: Bool
	var onClick: () -> Void
	
	init(title: String, enabled: Bool, onClick: @escaping () -> Void) {
		self.title = title
		self.enabled = enabled
		self.onClick = onClick
	}
	
	@objc func buttonClick() {
		onClick()
	}
	
}

class ButtonDrawer: CellDrawer {
	
	static var nib = UINib(nibName: "ButtonCell", bundle: nil)
	static var type = CellType.nib(nib, ButtonCell.self)
	
	static func draw(_ cell: ButtonCell, with item: ButtonItem) {
		cell.button.titleLabel?.text = item.title
		cell.button.isEnabled = item.enabled
		cell.button.addTarget(item, action: #selector(ButtonItem.buttonClick), for: .touchUpInside)
	}
}
