//
//  ButtonCell.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 7/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {

	@IBOutlet weak var button: UIButton!
    
}
