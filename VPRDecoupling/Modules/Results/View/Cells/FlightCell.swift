//
//  FlightCell.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 6/1/18.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import UIKit

class FlightCell: UITableViewCell {

	@IBOutlet weak var flightNumber: UILabel!
	@IBOutlet weak var origin: UILabel!
	@IBOutlet weak var destination: UILabel!
	@IBOutlet weak var departure: UILabel!
	@IBOutlet weak var arrival: UILabel!
	
}


