//
//  ResultsViewController.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import UIKit
import TableViewKit

class ResultsViewController: UIViewController {
	
	var tableViewManager: TableViewManager!
	@IBOutlet weak var tableView: UITableView!
	var presenter: ResultsPresenterProtocol!
	var viewModel: ResultsViewModel? {
		didSet {
			title = viewModel!.title
            tableViewManager = ResultsViewManager(tableView: tableView, delegate: self, viewModel: viewModel!)
		}
	}
    
    var resultsViewManager: ResultsViewManager? { return tableViewManager as? ResultsViewManager }

    override func viewDidLoad() {
        super.viewDidLoad()
		tableView.tableFooterView = UIView(frame: CGRect.zero)
		
        presenter.viewDidLoad()
    }
	
//	override func viewDidLayoutSubviews() {
//		if let rect = self.navigationController?.navigationBar.frame {
//			let y = rect.size.height + rect.origin.y
//			self.tableView.contentInset = UIEdgeInsetsMake( y, 0, 0, 0)
//		}
//	}

}

extension ResultsViewController: ResultsViewProtocol {
	
	func hideOutboundFlightsBut(_ flight: ResultsFlightViewModel) {
        resultsViewManager?.hideOutboundFlightsBut(flight)
	}
	
	func hideInboundFlightsBut(_ flight: ResultsFlightViewModel) {
        resultsViewManager?.hideInboundFlightsBut(flight)
	}
    
    func showAllOutboundFlights() {
        resultsViewManager?.showAllOutboundFlights()
    }
    
    func showAllInboundFlights() {
        resultsViewManager?.showAllInboundFlights()
    }
	
	func deselectOutboundFlight(_ flight: ResultsFlightViewModel) {
        resultsViewManager?.deselectOutboundFlight(flight)
	}
	
	func deselectInboundFlight(_ flight: ResultsFlightViewModel) {
        resultsViewManager?.deselectInboundFlight(flight)
	}
	
	func setContinueButtonEnabled(_ enabled: Bool) {
		resultsViewManager?.setContinueButtonEnabled(enabled)
	}

}

extension ResultsViewController: ResultsViewManagerDelegate {

	func flightSelected(bound: ResultsFlightBound, item: ResultsFlightViewModel) {
		switch bound {
		case .inbound:
			presenter.inboundFlightSelected(item)
		case .outbound:
			presenter.outboundFlightSelected(item)
		}
	}
    
    func continueButtonClick() {
        presenter.continueClicked()
    }
    
}
