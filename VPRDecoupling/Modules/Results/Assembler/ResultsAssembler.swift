//
//  ResultsAssembler.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//

import Foundation

class ResultsAssembler {
	
	static func makeModule(params: ResultsParams? = nil) -> BaseView {
		
		// Instantiation
        let view = ResultsViewController()
//        let view = ResultsCollectionViewController()
		let presenter = ResultsPresenter()
		let router = ResultsRouter()
		let interactor = ResultsInteractor()
		
		// View - Presenter
		view.presenter = presenter
		presenter.view = view
		
		// View/Presenter - Router
		router.view = view
		presenter.router = router
		
		// Presenter - Interactor
		presenter.interactor = interactor
		interactor.presenter = presenter
		
		// Params
		presenter.params = params
		
		return view
	}
	
}
