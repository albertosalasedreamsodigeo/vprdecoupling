//
//  HeaderAltView.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 11/01/2018.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import UIKit

class HeaderAltView: UICollectionReusableView {
    
    @IBOutlet weak var label: UILabel!
    
}
