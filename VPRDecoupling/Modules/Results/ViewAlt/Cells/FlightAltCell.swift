//
//  FlightAltCell.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 11/01/2018.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import UIKit

class FlightAltCell: UICollectionViewCell {

    @IBOutlet weak var flightNumber: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var departure: UILabel!
    @IBOutlet weak var arrival: UILabel!
    
    var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter
    }()
    
    var viewModel: ResultsFlightViewModel? {
        didSet {
            flightNumber.text = viewModel?.number
            origin.text = viewModel?.origin
            destination.text = viewModel?.destination
            departure.text = viewModel != nil ? dateFormatter.string(from: viewModel!.departure) : nil
            arrival.text = viewModel != nil ? dateFormatter.string(from: viewModel!.arrival) : nil
        }
    }

}
