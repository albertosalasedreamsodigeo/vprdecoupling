//
//  ResultsCollectionViewController.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 11/01/2018.
//  Copyright © 2018 Alberto Salas. All rights reserved.
//

import UIKit

private let flightReuseIdentifier = "Flight"
private let headerReuseIdentifier = "Header"
private let buttonReuseIdentifier = "Button"

class ResultsCollectionViewController: UICollectionViewController {
    
    var presenter: ResultsPresenterProtocol!
    var viewModel: ResultsViewModel? {
        didSet {
            title = viewModel!.title
            collectionView?.reloadData()
        }
    }
    var outboundSelected: ResultsFlightViewModel?
    var inboundSelected: ResultsFlightViewModel?
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
        UICollectionView.appearance().backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        let flightNib = UINib(nibName: "FlightAltCell", bundle:nil)
        self.collectionView!.register(flightNib, forCellWithReuseIdentifier: flightReuseIdentifier)
        let buttonNib = UINib(nibName: "ButtonAltCell", bundle:nil)
        self.collectionView!.register(buttonNib, forCellWithReuseIdentifier: buttonReuseIdentifier)
        
        let headerNib = UINib(nibName: "HeaderAltView", bundle:nil)
        self.collectionView!.register(headerNib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        
        presenter.viewDidLoad()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return outboundSelected != nil ? 1 : viewModel!.outbound!.flights!.count
        case 1:
            return inboundSelected != nil ? 1 : viewModel!.inbound!.flights!.count
        case 2:
            return 1
        default:
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: flightReuseIdentifier, for: indexPath) as! FlightAltCell
            if let flight = outboundSelected {
                cell.viewModel = flight
            } else {
                cell.viewModel = viewModel!.outbound!.flights![indexPath.row]
            }
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: flightReuseIdentifier, for: indexPath) as! FlightAltCell
            if let flight = inboundSelected {
                cell.viewModel = flight
            } else {
                cell.viewModel = viewModel!.inbound!.flights![indexPath.row]
            }
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: buttonReuseIdentifier, for: indexPath) as! ButtonAltCell
            cell.button.removeTarget(self, action: #selector(continueClicked), for: .touchUpInside)
            cell.button.addTarget(self, action: #selector(continueClicked), for: .touchUpInside)
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier, for: indexPath) as! HeaderAltView
        switch indexPath.section {
        case 0:
            header.label.text = "Outbound"
        case 1:
            header.label.text = "Inbound"
        default:
            header.label.text = nil
        }
        return header
    }
    
    // MARK: UICollectionViewDelegate
    
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            presenter.outboundFlightSelected(viewModel!.outbound!.flights![indexPath.row])
        case 1:
            presenter.inboundFlightSelected(viewModel!.inbound!.flights![indexPath.row])
        default:
            break
        }
    }
    
    // MARK: Button Target
    
    @objc func continueClicked() {
        presenter.continueClicked()
    }
    
}

extension ResultsCollectionViewController: ResultsViewProtocol {
    
    func hideOutboundFlightsBut(_ flight: ResultsFlightViewModel) {
        outboundSelected = flight
        collectionView?.reloadData()
    }
    
    func hideInboundFlightsBut(_ flight: ResultsFlightViewModel) {
        inboundSelected = flight
        collectionView?.reloadData()
    }
    
    func showAllOutboundFlights() {
        outboundSelected = nil
        collectionView?.reloadData()
    }
    
    func showAllInboundFlights() {
        inboundSelected = nil
        collectionView?.reloadData()
    }
    
    func deselectOutboundFlight(_ flight: ResultsFlightViewModel) {
        
    }
    
    func deselectInboundFlight(_ flight: ResultsFlightViewModel) {
        
    }
    
    func setContinueButtonEnabled(_ enabled: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(10)) { [weak self] in
            let indexPath = IndexPath(row: 0, section: 2)
            let buttonCell = self?.collectionView?.cellForItem(at: indexPath) as! ButtonAltCell
            buttonCell.button.isEnabled = enabled
        }
    }
    
    
}

extension ResultsCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0, 1:
            return CGSize(width: collectionView.bounds.width, height: 100)
        case 2:
            return CGSize(width: collectionView.bounds.width, height: 70)
        default:
            return CGSize.zero
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case 0, 1:
            return CGSize(width: collectionView.bounds.width, height: 30)
        default:
            return CGSize.zero
        }
    }
    
}
