//
//  ResultsInteractor.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class ResultsInteractor {

    weak var presenter: ResultsInteractorOutputProtocol!
    
}

extension ResultsInteractor: ResultsInteractorInputProtocol {
	
	func fetchFlights() {
		let outboundFlights = getOutboundFlights()
		let inboundFlights = getInboundFlights()
		self.presenter.didReceiveFlights((outboundFlights, inboundFlights))
	}

}

extension ResultsInteractor {
	
	func getOutboundFlights() -> [Flight] {
		let oflight1 = Flight(number: "1234", origin: "BCN", destination: "NYC", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		let oflight2 = Flight(number: "1235", origin: "BCN", destination: "NYC", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		let oflight3 = Flight(number: "1236", origin: "BCN", destination: "NYC", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		return [oflight1, oflight2, oflight3]
	}
	
	func getInboundFlights() -> [Flight] {
		let iflight1 = Flight(number: "5678", origin: "NYC", destination: "BCN", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		let iflight2 = Flight(number: "5679", origin: "NYC", destination: "BCN", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		let iflight3 = Flight(number: "5670", origin: "NYC", destination: "BCN", departure: Date(), arrival: Date(), moreData: nil, evenMoreData: nil)
		return [iflight1, iflight2, iflight3]
	}
}
