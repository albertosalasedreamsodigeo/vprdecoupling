//
//  ResultsViewModel.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

class ResultsViewModel {
	
	var title: String?
	var outbound: ResultsFlightListViewModel?
	var inbound: ResultsFlightListViewModel?
	
    init(title: String?, outbound: ResultsFlightListViewModel?, inbound: ResultsFlightListViewModel?) {
        self.title = title
        self.outbound = outbound
        self.inbound = inbound
    }

}

class ResultsFlightViewModel: Equatable {
	
	let number: String
	let origin: String
	let destination: String
	let departure: Date
	let arrival: Date
	
	init(number: String, origin: String, destination: String, departure: Date, arrival: Date) {
		self.number = number
		self.origin = origin
		self.destination = destination
		self.departure = departure
		self.arrival = arrival
	}
	
	static func ==(lhs: ResultsFlightViewModel, rhs: ResultsFlightViewModel) -> Bool {
		return lhs.number == rhs.number && lhs.origin == rhs.origin && lhs.destination == rhs.destination && lhs.departure == rhs.departure && lhs.arrival == rhs.arrival
	}
    
}

class ResultsFlightListViewModel {
    
	let header: String?
	let footer: String?
	let flights: [ResultsFlightViewModel]?
    
    init(header: String?, footer: String?, flights: [ResultsFlightViewModel]?) {
        self.header = header
        self.footer = footer
        self.flights = flights
    }
    
}
