//
//  ResultsContract.swift
//  VPRDecoupling
//
//  Created by Alberto Salas on 16/12/17.
//  Copyright © 2017 Alberto Salas. All rights reserved.
//
//  Generated using ASVIPERTemplate v0.2 https://github.com/albsala/ASVIPERTemplate/
//

import Foundation

//MARK: - Router
protocol ResultsRouterProtocol: class {
    func showSummary(outboundFlight: Flight, inboundFlight: Flight)
    func showError(message: String)
}

//MARK: - View
protocol ResultsViewProtocol: BaseView {
	var viewModel: ResultsViewModel? { get set }
	func hideOutboundFlightsBut(_ flight: ResultsFlightViewModel)
	func hideInboundFlightsBut(_ flight: ResultsFlightViewModel)
    func showAllOutboundFlights()
    func showAllInboundFlights()
	func deselectOutboundFlight(_ flight: ResultsFlightViewModel)
	func deselectInboundFlight(_ flight: ResultsFlightViewModel)
	func setContinueButtonEnabled(_ enabled: Bool)
}

//MARK: - Presenter
protocol ResultsPresenterProtocol: class {
	func viewDidLoad()
	func outboundFlightSelected(_ flight: ResultsFlightViewModel)
	func inboundFlightSelected(_ flight: ResultsFlightViewModel)
	func continueClicked()
}

//MARK: - Interactor
protocol ResultsInteractorInputProtocol: class {
	func fetchFlights()
}

protocol ResultsInteractorOutputProtocol: class {
	func didReceiveFlights(_ flights: ([Flight], [Flight]))
}
